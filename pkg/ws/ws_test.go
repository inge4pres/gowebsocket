package ws

import (
	"context"
	"github.com/stretchr/testify/assert"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"testing"
	"time"
)

func Test_aWebsocketConnectionSendThroughClient(t *testing.T) {
	//w := httptest.NewRecorder()
	server := httptest.NewServer(http.HandlerFunc(random))
	defer server.Close()

	sendNumber := rand.Int63()
	req := []byte(strconv.Itoa(int(sendNumber)))
	ctx, _ := context.WithDeadline(context.Background(), time.Now().Add(time.Second*1))

	rawUrl, _ := url.Parse(server.URL)
	wsUrl := url.URL{Scheme: "ws", Host: rawUrl.Host}

	resp, err := NewClient(wsUrl.String()).Send(ctx, req)
	assert.Nil(t, err)

	recvNumber, _ := strconv.Atoi(string(resp))
	assert.True(t, sendNumber >= int64(recvNumber))
}
