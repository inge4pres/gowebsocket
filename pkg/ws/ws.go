package ws

import (
	"context"
	"errors"
	"github.com/gorilla/websocket"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

var upgrader = websocket.Upgrader{
	HandshakeTimeout: time.Duration(time.Second * 2),
}

type Client struct {
	serverAddr string
}

func random(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("upgrade to WS error: %v", err)
		return
	}
	defer c.Close()
	for {
		mt, message, err := c.ReadMessage()
		if err != nil {
			log.Printf("error reading from client: %v", err)
			break
		}
		if mt == websocket.CloseMessage {
			log.Printf("client said goodbye")
			break
		}
		log.Printf("received message type %d with content: %s", mt, message)

		number, err := strconv.Atoi(string(message))
		if err != nil {
			log.Printf("message received is not a number: %s, error: %v", message, err)
			break
		}
		if err = c.WriteMessage(mt, []byte(strconv.Itoa(int(rand.Int63n(int64(number)))))); err != nil {
			log.Printf("error writing back to client: %v", err)
			break
		}
	}
	return
}


func (c *Client) Send(ctx context.Context, body []byte) ([]byte, error) {
	conn, _, err := websocket.DefaultDialer.Dial(c.serverAddr, nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer conn.Close()
	incoming := make(chan []byte)
	go func() {
		for {
			_, message, err := conn.ReadMessage()
			if err == websocket.ErrCloseSent{
				return
			}
			if err != nil {
				log.Println("read error:", err)
				break
			}
			incoming <- message
		}
	}()

	go func() {
		if err := conn.WriteMessage(websocket.TextMessage, body); err != nil {
			log.Printf("error writing to server: %v", err)
			return
		}
		conn.WriteMessage(websocket.CloseMessage, nil)
	}()

	select {
	case <-ctx.Done():
		log.Printf("request canceled")
		return nil, errors.New("deadline")
	case msg := <-incoming:
		log.Printf("[CLIENT] received from server: %s", string(msg))
		return msg, nil
	}
}

func NewClient(url string) *Client {
	return &Client{url}
}
